﻿console.log('site.js');
if ('serviceWorker' in navigator) {

    
    navigator.serviceWorker.register(location.origin+'/js/sw.js')
        .then(() => navigator.serviceWorker.ready.then((worker) => {
            console.log('serviceWorker is ready');
            worker.sync.register('syncdata');
        }))
        .catch((err) => console.log(err));
}