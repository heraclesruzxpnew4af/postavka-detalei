﻿using Auth;

using Microsoft.AspNetCore.Http;

using System;
using System.Threading.Tasks;

using static System.Console;

public class AppMiddle
{
    private readonly RequestDelegate _next;

  


    public AppMiddle(RequestDelegate next)
    {
        _next = next;

    
    }
    public string GetToken(HttpContext http)
    {
        if (http == null || http.Request == null)
        {
            return null;
        }
        return http.Request.Cookies.ContainsKey("Authorization") == false ? null :
               http.Request.Cookies["Authorization"].ToString();
    }
    public async Task Invoke(HttpContext http, IAuthOptions authOptions, IAuthContext _context)
    { 

        string url = http.Request.Path.ToString();
        if (url.Length == 0)
        {
            await _next.Invoke(http);
        }
        else
        {
            var ids = url.Substring(1).Split("/");
            if (ids.Length > 2)
            {

                string area = ids[0];
                switch (area)
                {
                    case "User":
                        if (_context.IsSignIn(GetToken(http)) == false)
                        {
                            WriteLine(authOptions.LoginPath);
                            http.Response.Redirect(authOptions.LoginPath);

                        }


                        break;
                    default:
                        if (_context.IsUserInRole(GetToken(http), area) == false)
                        {
                            WriteLine(authOptions.LoginPath);
                            http.Response.Redirect(authOptions.LoginPath);
                        }

                        break;
                }

            }



        } 
        await _next.Invoke(http);


    }

    private void WriteError(HttpContext http, Exception ex)
    {
        http.Response.StatusCode = 500;
        http.Response.WriteAsync("<div class='alert alert-error'>" + ex.Message + "</div>");
        http.Response.WriteAsync("<div class='alert alert-warning'>" + ex.StackTrace + "</div>");
    }

}