﻿using Auth.Data;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Views;
using UserAuthMVC.Views.Registration;

namespace Views.Registration
{
    public class RegistrationController : Controller
    {
        private readonly IRegistration _registration;

        public RegistrationController( IRegistration registration )
        {
            _registration = registration;
        }

        public bool ToDo(RegistrationModel Model) => _registration.CreateUserAccount(Model);

        public IActionResult Index() => RedirectToAction("RegistrationView");

        [HttpGet]
        public IActionResult RegistrationView()
        {
            return View(new RegistrationModel());
        }


        [HttpPost]
        public IActionResult RegistrationView(RegistrationModel Model)
        {
            
            if (ModelState.IsValid)
            {
                bool Success = ToDo(Model);
                if (Success)
                {
                    return RedirectToAction("Index","Login");
                }
                else
                {
                    ModelState.AddModelError("Email","Регистрация не выполнена");
                    return View(Model);
                }
            }
            else
            {
                return View(Model);
            }
            
        }

        
    }
}
