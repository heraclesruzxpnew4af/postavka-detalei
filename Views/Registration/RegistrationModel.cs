﻿using Auth.Data;

using Microsoft.AspNetCore.Mvc;

using ReCaptcha;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Views.Registration
{
    [Display(Name = "Регистрация")] 
    public class RegistrationModel : Account
    {


        [RecaptchaValidation("Подтвердите что вы не робот")]
        [FromForm(Name = "g-recaptcha-response")]
        [BindProperty(Name = "g-recaptcha-response")]
        [Display(Name = "g-Recaptcha-Response")]
        public string ReCaptcha { get; set; }


        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Необходимо ввести пароль")]
        [DataType(DataType.Password)]
        [StringLength(26, MinimumLength = 8)]
        public string Password { get; set; } = "eckumoc@gmail.com";

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение")]
        [Compare("Password", ErrorMessage = "Пароль отличается от подтверждения")]
        public string Confirmation { get; set; } = "eckumoc@gmail.com";

        [Display(Name = "Полное имя")]
        [Required]
        public string Name { get; set; } = "Константин";

        [Display(Name = "Отчество")]
        [Required]
        public string Patronymic { get; set; } = "Александрович";

        [Display(Name = "Фамилия")]
        [Required]
        public string Surname { get; set; } = "Батов";

        [Display(Name = "Пол")]
        public bool IsWoman { get; set; } = false;

        [Display(Name = "Дата рождения")]
        [Required]
        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; } = DateTime.Parse("26.08.1989");
    }
}