﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Views.Registration;

namespace Views.Registration
{
    internal class RegistrationModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<IRegistration, RegistrationService>();
        }
    }
}
