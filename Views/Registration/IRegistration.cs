﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Views.Registration;

namespace UserAuthMVC.Views.Registration
{
    public interface IRegistration
    {

        public bool CreateUserAccount(RegistrationModel Model);
    }
}
