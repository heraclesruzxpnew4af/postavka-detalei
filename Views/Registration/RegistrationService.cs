﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Auth.Data;
using Auth.Services.AuthRepositories;
using Auth.Services.UserManager;

using Data;

using Views.Registration;

namespace UserAuthMVC.Views.Registration
{
    public class RegistrationService: IRegistration
    {
        private readonly IUserManager _userManager;

        public RegistrationService(IUserManager userManager)
        {
            _userManager = userManager;
            
        }

        public bool CreateUserAccount(RegistrationModel Model)
        {
            int result = _userManager.CreateUser(
                    Model.Email, Model.Phone, Model.Password,
                    Model.Name, Model.Surname, Model.Patronymic, Model.IsWoman, Model.Birthday);
            return result > 0;
        }
    }
}
