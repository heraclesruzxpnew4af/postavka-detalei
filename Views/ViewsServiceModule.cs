﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MVC.Album.Interfaces;
using MVC.Album.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Views.Login;
using Views.Logout;
using Views.Registration;

namespace Views
{
    internal class ViewsServiceModule: IServiceModule
    {
        private LoginModule LoginModule = new LoginModule();
        private RegistrationModule RegistrationModule = new RegistrationModule();
        private LogoutModule LogoutModule = new LogoutModule();
        
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<IStoredImageRepository, StoredImageRepository>();
            LogoutModule.ConfigureServices(configuration, services);
            LoginModule.ConfigureServices(configuration, services);
            RegistrationModule.ConfigureServices(configuration, services);
        }
    }
}
