﻿using Microsoft.AspNetCore.Mvc;

using ReCaptcha;

using System.ComponentModel.DataAnnotations;

namespace Views.Login
{
    public class LoginModel: Auth.Data.Login
    {

        [RecaptchaValidation("Подтвердите что вы не робот")]
        [FromForm(Name = "g-recaptcha-response")]
        [BindProperty(Name = "g-recaptcha-response")]
        [Display(Name = "g-Recaptcha-Response")]
        public string ReCaptcha { get; set; }
    }
}