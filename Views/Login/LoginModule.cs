﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Login
{
    public class LoginModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<ILogin, LoginService>();
        }
    }
}
