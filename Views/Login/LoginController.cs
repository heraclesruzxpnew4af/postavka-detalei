﻿using Auth;
using Auth.Data;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Views;

namespace Views.Login
{
 
    public class LoginController : Controller
    {
        private readonly ILogin _login;

        private string GetIpv4() => HttpContext.Connection.RemoteIpAddress.ToString();
        public UserContext ToDo(Auth.Data.Login Model) => _login.UserLogin(Model);


        public LoginController(ILogin login)
        {
            _login = login;
        }

        public IActionResult Index()
        {
         
            return RedirectToAction("LoginView" );
        }

        [HttpGet]
        public IActionResult LoginView()
        {
            ToDo(new LoginModel()
            {
                UserID = "eckumoc@gmail.com",
                Password = "eckumoc@gmail.com"

            });
            return RedirectToAction("Index", "Home", new { area = "User" });
           

        }

        [HttpPost]
        public IActionResult LoginView(LoginModel Model)
        {            
            //Model.IPv4 = GetIpv4();
            if (ModelState.IsValid)
            {                 
                var user = ToDo(Model);
                if(user == null)
                {
                    ModelState.AddModelError("UserID","Учетные данные не зарегистрированы");
                    return View(Model);
                }
                else
                {
                    ViewData["User"] = user;
                    return RedirectToAction("Index", "Home", new { area = "User" });
                }
            }
            else
            {
                return View(Model);
            }
            
        }

    }
}
