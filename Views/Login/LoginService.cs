﻿using Auth;
using Auth.Data;
using Auth.Services.AuthRepositories;
using Auth.Services.SigninManager;
using Auth.Services.UserManager;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Login
{
    internal class LoginService: ILogin
    {
        private readonly ILoginRepository _loginRepository;
        private readonly IUserManager _userManager;
        private readonly ISigninManager _signinManager;

        public LoginService(ILoginRepository loginRepository, IUserManager userManager, ISigninManager signinManager)
        {
            _loginRepository = loginRepository;
            _userManager = userManager;
            _signinManager = signinManager;
        }

 

        public UserContext UserLogin(Auth.Data.Login Model)
        {
            _loginRepository.Post(Model);

            User user = _userManager.FindByEmail(Model.UserID);
            user = user == null ? _userManager.FindByPhone(Model.UserID): user;
            if( user != null)
            {
                return _signinManager.Signin(user);
            }
            else
            {
                return null;
            }
            
        }
    }
}
