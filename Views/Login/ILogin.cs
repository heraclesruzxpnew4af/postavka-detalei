﻿using Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Login
{
    public interface ILogin
    {
        public UserContext UserLogin(Auth.Data.Login Model);
    }
}
