﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


public interface IStoredImageRepository  
{
    public Task<int> Create(StoredImage image);
    public Task<StoredImage[]> GetAll();
    public Task<StoredImage> GetByID(int iD);
    public Task<int> Remove(int iD);
    public Task<int> Update(StoredImage model);
}
namespace MVC.Album.Interfaces
{ }
