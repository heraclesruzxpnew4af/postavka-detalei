﻿using Data;

using Microsoft.EntityFrameworkCore;
 

 
using MVC.Album.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


public class StoredImage
{
    [Key]

    public int ID { get; set; }

    public string ContentType { get; set; }

    [Display(Name = "Файл")]
    public byte[] Data { get; set; }


    [Required(ErrorMessage = "Необходимо указать наименование")]
    [Display(Name = "Название")]
    public string Name { get; set; }


    [Display(Name = "Описание")]
    [DataType(DataType.MultilineText)]
    public string Desc { get; set; }


}

namespace MVC.Album.Services
{
    public class StoredImageRepository: IStoredImageRepository 
    {
        private readonly ApplicationDbContext _context;

        public StoredImageRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Create(StoredImage image)
        {
            _context.StoredImages.Add(image);
            return await _context.SaveChangesAsync();            
        }

        public Task<StoredImage[]> GetAll()
        {
            return Task.Run(() => {
                return _context.StoredImages.ToArray();
            });
        }

        public async Task<StoredImage> GetByID(int ID)
        {
            return await _context.StoredImages.Where(i => i.ID == ID).FirstOrDefaultAsync();
        }

        public async Task<int> Remove(int ID)
        {
            var image = await GetByID(ID);
            if(image == null)
            {
                return 0;
            }
            else
            {
                _context.StoredImages.Remove(image);
                return await _context.SaveChangesAsync();
            }
            
        }

        public async Task<int> Update(StoredImage image)
        {
         
            if (image == null)
            {
                return 0;
            }
            else
            {
                _context.StoredImages.Update(image);
                return await _context.SaveChangesAsync();
            }
        }
    }
}
