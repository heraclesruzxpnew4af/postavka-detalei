﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Logout
{
    public interface ILogout
    {
        public bool Logout();
    }
}
