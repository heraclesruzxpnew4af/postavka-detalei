﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Logout
{
    public class LogoutController : Controller
    {
        private readonly ILogout _logout;

        public LogoutController(ILogout logout)
        {
            _logout = logout;
        }

        public IActionResult Index()
        {
            _logout.Logout();
            return RedirectToAction("Index", "Login");
        }
    }
}
