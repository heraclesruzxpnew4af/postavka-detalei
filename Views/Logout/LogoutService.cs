﻿using Auth.Services.SigninManager;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Logout
{
    public class LogoutService: ILogout
    {
        private readonly ISigninManager _signinManager;

        public LogoutService(ISigninManager signinManager)
        {
            _signinManager = signinManager;
        }

        public bool Logout()
        {
            return _signinManager.Signout();
        }
    }
}
