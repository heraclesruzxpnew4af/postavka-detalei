﻿ 
using Auth.Fasade;
using Auth.Services.UserManager;
using Auth.Services.UserMessager;

using Data;
using Data.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

 

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Views.Registration;

namespace ProxyGateway.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly INewsRepository _news;

        public HomeController(IUserMessager messager, IRegistration registration, IUserManager userManager, INewsRepository news,   ApplicationDbContext context)
        {
 
            _context = context;
            _news = news;



            _context.Database.EnsureDeleted();
             _context.Database.EnsureCreated();
             AppDbInitiallizer.Init(_context);
            if (_context.Users.Count() == 0)
            {


                int userId = userManager.CreateUser("eckumoc@gmail.com",
                    "7-904-334-1124", "eckumoc@gmail.com", "Батов", "Константин", "Александрович", false, DateTime.Now);

                messager.SendMessage(userId, userId, new Auth.Data.Message() { Text = "This is a test" });
            } 



        }




        public IActionResult Index( SearchParams pars)
        {
            return View( _news.Search(pars));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        


    }
}
