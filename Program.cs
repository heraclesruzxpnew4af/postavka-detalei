using Auth;
using Auth.Services.UserManager;
using Auth.Services.UserMessager;

using Data;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using UserAuthMVC.Views.Registration;

using Views;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSingleton(typeof(IAuthOptions),sp=>new AuthOptions());
var services = builder.Services;
services.AddHttpClient();
services
    .AddControllersWithViews() ;
new AppServiceModule().ConfigureServices(builder.Configuration, services);
new AuthWorkerModule().ConfigureServices(builder.Configuration, services);
new ViewsServiceModule().ConfigureServices(builder.Configuration, services);

var app = builder.Build();
var provider = app.Services;

app.UseDeveloperExceptionPage();
 
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseMiddleware<AppMiddle>();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "default",
    pattern: "{area=}/{controller=Home}/{action=Index}/{id?}");
app.Run();
