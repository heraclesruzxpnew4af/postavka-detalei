﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


internal interface IServiceModule
{
    public void ConfigureServices(IConfiguration configuration, IServiceCollection services);
}

