﻿
using App.Areas.User.Data;

using Auth;
using Auth.Data;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

public class ApplicationDbContextSeed
{
    public static async Task SeedAsync(Data.ApplicationDbContext db)
    {
        throw new NotImplementedException();
    }
}
namespace Data
{


    /// <summary>
    /// Контекст базы данных EF
    /// </summary>
    public partial class ApplicationDbContext :  DbContext, IAuthDbContext
    {


        public virtual DbSet<UserWallet> Wallets { get; set; }
        public virtual DbSet<FinOperation> Operations { get; set; }
        public virtual DbSet<StoredImage> StoredImages { get; set; }
        public virtual DbSet<News> News { get; set; }

        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
      
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
     

}
