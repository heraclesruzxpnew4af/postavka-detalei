﻿using Data;
using Data.EntityTypes;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using static Newtonsoft.Json.JsonConvert;

namespace Auth.Fasade
{

    /// <summary>
    /// 
    /// </summary>
    public class SearchParams
    {
        public int Page { get; set; } = 1;
        public int Size { get; set; } = 3;
        public string Query { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class SearchResults<TEntity>: SearchParams
    {
        public IEnumerable<TEntity> Results { get; set; } = new List<TEntity>();
        public IEnumerable<string> Options { get; set; } = new List<string>();
        public int TotalResults { get; set; } = 1;
        public int TotalPages { get; set; } = 1;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IEntitySearch<TEntity> where TEntity: BaseEntity
    {

        public IEnumerable<TEntity> Search(string query, int page, int size);
        public SearchResults<TEntity> Search(SearchParams searchParams);
        public IEnumerable<TEntity> GetPage(int page, int size);
    }



    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class EntitySearch<TEntity>: EntityRepository<TEntity>, IEntitySearch<TEntity> where TEntity : BaseEntity
    {
     

        public EntitySearch(ApplicationDbContext context):base(context)
        {
             
        }
        public IEnumerable<TEntity> GetPage(int page, int size)
            => _context.Set<TEntity>().Skip((page - 1) * size).Take(size).ToArray();

        public override DbSet<TEntity> GetDbSet(ApplicationDbContext context)
            => context.Set<TEntity>();

        public abstract IEnumerable<TEntity> Search(string query, int page, int size);
        public abstract SearchResults<TEntity> Search(SearchParams searchParams);

     
    }

}
