﻿using Auth;

using System;

namespace Data
{
    public class AppDbInitiallizer
    {
        private static void CreateNews(ApplicationDbContext db)
        {
            db.News.Add(new News()
            {
                Title = "Новая версия 1 ",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.News.Add(new News()
            {
                Title = "Новая версия 2 ",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.News.Add(new News()
            {
                Title = "Новая версия 3 ",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.News.Add(new News()
            {
                Title = "Новая версия 4 ",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.News.Add(new News()
            {
                Title = "Новая версия 5",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.News.Add(new News()
            {
                Title = "Новая версия",
                Text = "Новая версия рсширяет функционал системы.",
                Href = "/"
            });
            db.SaveChanges();
        }

        private static void CreateRole(ApplicationDbContext db)
        {
            db.Roles.Add(new Auth.Data.Role("User") );
            db.SaveChanges();
        }

        internal static void Init(ApplicationDbContext db)
        {
            AppDbInitiallizer.CreateRole(db);
            AppDbInitiallizer.CreateUser(db);
            AppDbInitiallizer.CreateNews(db);
        }

        private static void CreateUser(ApplicationDbContext db)
        {
            
        }
    }
}
