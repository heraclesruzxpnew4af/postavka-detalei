﻿using Data.EntityTypes;
 
public class News: BaseEntity
{
    public string Title { get; set; }
    public string Text { get; set; }
    public string Href { get; set; }
}
 