﻿ 

using Auth.Fasade;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Services
{
    public interface INewsRepository: IEntityRepository<News>, IEntitySearch<News>
    {
        public IEnumerable<News> GetTopRated();
        
    }
    public class NewsRepository: EntitySearch<News>, INewsRepository
    {
        public NewsRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public override DbSet<News> GetDbSet(ApplicationDbContext context)
            => context.News;

        public IEnumerable<News> GetTopRated()
            => this.Get(null);

        public override IEnumerable<News> Search(string query, int page, int size)
            => GetDbSet(_context).Where(record=> record.Text.ToLower().IndexOf(query.ToLower())!=-1).ToArray();

        public override SearchResults<News> Search(SearchParams pars)
        {
            if (pars.Query == null) pars.Query = "";
            var records = GetDbSet(_context).Where(record => record.Text.ToLower().IndexOf(pars.Query.ToLower()) != -1);
            var results = new SearchResults<News>()
            {
                Page = pars.Page,
                Size = pars.Size,
                Query = pars.Query,
                Results = records.Skip(pars.Size*(pars.Page-1)).Take(pars.Size).ToArray(),
                TotalPages = records.Count()%pars.Size!=0? ((int)(Math.Floor((decimal)(records.Count()/pars.Size))+1)): 
                ((int)(Math.Floor((decimal)(records.Count() / pars.Size)) )),
                TotalResults = records.Count()
            };

            return results;
        }
    }
}
