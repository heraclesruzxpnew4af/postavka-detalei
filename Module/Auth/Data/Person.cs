﻿using Data.EntityTypes;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Data
{
    [Table("Person",Schema = "Auth")]    
    public class Person: BaseEntity
    {
 

        [Display(Name = "Полное имя")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Отчество")]
        [Required]
        public string Patronymic { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string Surname { get; set; }

        [Display(Name = "Пол")]
        public bool IsWoman { get; set; }

        [Display(Name = "Дата рождения")]
        [Required]
        [Column(TypeName = "date")]        
        public DateTime? Birthday { get; set; }


        public int? UserID { get; set; }

        public Person()
        {
        }

        public Person(string name, string patronymic, string surname, bool isWoman, DateTime? birthday, int? userID)
        {
            Name = name;
            Patronymic = patronymic;
            Surname = surname;
            IsWoman = isWoman;
            Birthday = birthday;
            UserID = userID;
        }
    }
}
