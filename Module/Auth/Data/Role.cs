﻿using Data.EntityTypes;

namespace Auth.Data
{
    public class Role: DimTable
    {
        public Role(string code)
        {
            Code = code;
        }    
    }


    public class UserRole : BaseEntity
    {
         
       
        public int UserID { get; set; }
        public int RoleID { get; set; }

    }
}