﻿using Data.EntityTypes;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Data
{
    [Display(Name = "Авторизация")]
    [Table("Login", Schema = "Auth")]
    public class Login: BaseEntity
    {
        public Login()
        {
        }

        public Login(string userID, string password, DateTime time, bool rememberMe)
        {
            UserID = userID;
            Password = password;
            Time = time;
            RememberMe = rememberMe;
        }


        //[Display(Name = "IP-адрес соединения")]
        //[Required(ErrorMessage = "Не задан адрес соединения")]
        //public string IP { get; set; }

        [Display(Name = "Номер телефона/Электронная почта")]
        [Required(ErrorMessage = "Укажите номер телефона или адрес электронной почты")]
        public string UserID { get; set; }

        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Укажите пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Время операции")]
        [Required(ErrorMessage = "Не установлено время выполнения операции")]
        [Column(TypeName = "datetime")]
        public DateTime Time { get; internal set; } = DateTime.Now;

        [Display(Name = "Запомнить меня")]        
        public bool RememberMe { get; set; }
    }
}
