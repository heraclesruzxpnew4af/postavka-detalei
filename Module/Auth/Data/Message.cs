﻿using Data.EntityTypes;

namespace Auth.Data
{
    public class Message: BaseEntity
    {
        public Message()
        {
        }

        public Message(string subject, string text, int? fromUserID, int? toUserID, User? fromUser, User? toUser)
        {
            Subject = subject;
            Text = text;
            FromUserID = fromUserID;
            ToUserID = toUserID;
            FromUser = fromUser;
            ToUser = toUser;
        }

        public string Subject { get; set; } = "";
        public string Text { get; set; }
        public virtual int? FromUserID { get; set; } = 1;
        public virtual int? ToUserID { get; set; } = 1;
        public virtual User? FromUser  { get; set; }
        public virtual User? ToUser  { get; set; }
    }
}
