﻿using Auth.Data;

using Data.EntityTypes;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Data
{
    public class User: BaseEntity
    {
        public User()
        {
        }

        public User(int? accountID, Account account, int? personID, Person person, ICollection<Message> inbox, ICollection<Message> outbox, ICollection<UserRole> userRoles, ICollection<Role> roles)
        {
            AccountID = accountID;
            Account = account;
            PersonID = personID;
            Person = person;
            Inbox = inbox;
            Outbox = outbox;
            UserRoles = userRoles;
            Roles = roles;
        }

        public virtual int? AccountID { get; set; }
        public virtual Account Account { get; set; }
        public virtual int? PersonID { get; set; }
        public virtual Person Person { get; set; }

        [InverseProperty("FromUser")]
        public ICollection<Message> Inbox { get; set; }

        [InverseProperty("ToUser")]
        public ICollection<Message> Outbox { get; set; }
        
        public ICollection<UserRole> UserRoles { get; set; }

        [NotMapped]
        public ICollection<Role> Roles { get; set; } = new List<Role>();
    }
}
