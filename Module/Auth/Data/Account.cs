﻿using Data.EntityTypes;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Data
{
 
    [Display(Name = "Учетная запись пользователя")]
    [Table("Account", Schema = "Auth")]
    public class Account: BaseEntity
    {


        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Необходимо ввести номер телефона")]
        public string Phone { get; set; } = "7-904-334-1124";

        [Display(Name = "Адрес электронной почты")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Необходимо ввести адрес электронной почты")]
        public string Email { get; set; } = "eckumoc@gmail.com";
        public string HashKey { get; set; } = "";

        

    }
}
