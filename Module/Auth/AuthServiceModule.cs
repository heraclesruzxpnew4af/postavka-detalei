﻿using Auth.Services;
using Auth.Singleton;

using Data;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth
{
    internal class AuthServiceModule: IServiceModule
    {
        private AuthServicesModule AuthServicesModule = new AuthServicesModule();
        private DataServiceModule DataServiceModule = new DataServiceModule();
        private AuthSingletonModule AuthSingletonModule = new AuthSingletonModule();
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            
            DataServiceModule.ConfigureServices(configuration, services);
            AuthSingletonModule.ConfigureServices(configuration, services);
            AuthServicesModule.ConfigureServices(configuration, services);
        }
    }
}
