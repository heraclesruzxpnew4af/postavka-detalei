﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReCaptcha
{
    public class ReCaptchaModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddSingleton(typeof(ReCaptchaOptions), sp => {
                var options = configuration.GetSection(nameof(ReCaptchaOptions)).Get<ReCaptchaOptions>();
                return options;
            });
            services.AddScoped<ReCaptchaService>();
        }
    }
}
