﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserAuthMVC.Auth.Services.TokenProvider
{
    public class HttpRequestHeaderTokenProvider: ITokenProvider
    {
        private readonly IHttpContextAccessor _accessor;

        public HttpRequestHeaderTokenProvider(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Get()
        {
            if(_accessor.HttpContext == null || _accessor.HttpContext.Request==null || _accessor.HttpContext.Request.Headers == null)
            {
                return null;
            }
            return _accessor.HttpContext.Request.Headers.ContainsKey("Authorization") == false? null:
                   _accessor.HttpContext.Request.Headers["Authorization"].ToString();
        }

        public void Set(string token)
        {
        }
    }
}
