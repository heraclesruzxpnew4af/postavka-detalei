﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Auth.Services;

namespace Auth.Services.TokenProvider
{
    public class CookieTokenProvider: ITokenProvider
    {
        private IHttpContextAccessor _accessor = null;

        public CookieTokenProvider(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Get()
        {
            if (_accessor.HttpContext == null || _accessor.HttpContext.Request == null)
            {
                return null;
            }
            return _accessor.HttpContext.Request.Cookies.ContainsKey("Authorization") == false ? null :
                   _accessor.HttpContext.Request.Cookies["Authorization"].ToString();
        }

        public void Set(string token)
        {
            if (_accessor.HttpContext != null  )
            {
                
        
                _accessor.HttpContext.Response.Cookies.Append("Authorization",token);
            }
           
        }
    }
}
