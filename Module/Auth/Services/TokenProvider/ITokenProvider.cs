﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserAuthMVC.Auth.Services
{
    public interface ITokenProvider
    {
        public string Get();
        public void Set(string token);
    }
}
