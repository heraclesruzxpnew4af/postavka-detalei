﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Auth.Services;
using UserAuthMVC.Auth.Services.TokenProvider;

namespace Auth.Services.TokenProvider
{
    internal class TokenProviderModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            if( services.Where(desc=>desc.ServiceType.Name==nameof(IHttpContextAccessor)).Count()==0)
            {
                services.AddHttpContextAccessor();
            }
            services.AddScoped<ITokenProvider, CookieTokenProvider>();
        }
    }
}
