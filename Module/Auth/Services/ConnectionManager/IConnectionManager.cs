﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Auth.Services.ConnectionManager
{
    public interface IConnectionManager
    {
        public Task<X509Certificate2> GetCertificate();
    }
}
