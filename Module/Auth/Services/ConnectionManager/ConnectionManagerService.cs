﻿using Auth.Services.AuthCertificate;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Auth.Services.ConnectionManager
{
    public class ConnectionManagerService: IConnectionManager
    {
        private readonly IAuthCretificate _authCretificate;

        public ConnectionManagerService(IAuthCretificate authCretificate)
        {
            _authCretificate = authCretificate;
        }
        public async Task<X509Certificate2> GetCertificate() => await _authCretificate.GetCertificate();
    }
}
