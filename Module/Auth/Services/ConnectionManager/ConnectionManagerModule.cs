﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.ConnectionManager
{
    internal class ConnectionManagerModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<IConnectionManager, ConnectionManagerService>();
        }
    }
}
