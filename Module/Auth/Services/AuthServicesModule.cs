﻿using Auth.Services.AuthCertificate;
using Auth.Services.AuthRepositories;
using Auth.Services.ConnectionManager;
using Auth.Services.SigninManager;
using Auth.Services.TokenProvider;
using Auth.Services.UserManager;
using Auth.Services.UserMessager;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services
{
    internal class AuthServicesModule: IServiceModule
    {
        private AuthCertificateModule AuthCertificateModule = new AuthCertificateModule();
        private TokenProviderModule TokenProviderModule = new TokenProviderModule();
        private AuthRepositoriesModule AuthRepositoriesModule = new AuthRepositoriesModule();
        private SigninManagerModule SigninManagerModule = new SigninManagerModule();
        private UserManagerModule UserManagerModule = new UserManagerModule();
        private ConnectionManagerModule ConnectionManagerModule = new ConnectionManagerModule();
        private UserMessagerModule UserMessagerModule = new UserMessagerModule();
        


        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            UserMessagerModule.ConfigureServices(configuration, services);
            AuthCertificateModule.ConfigureServices(configuration, services);
            TokenProviderModule.ConfigureServices(configuration, services);
            AuthRepositoriesModule.ConfigureServices(configuration, services);
            SigninManagerModule.ConfigureServices(configuration, services);
            UserManagerModule.ConfigureServices(configuration, services);
            ConnectionManagerModule.ConfigureServices(configuration,services);
        }
    }
}
