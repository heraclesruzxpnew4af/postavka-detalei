﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;

namespace Auth.Services.UserMessager
{
    public interface IUserMessager : IEntityRepository<Message>
    {
        public IEnumerable<Message> GetInbox(int userId);
        public IEnumerable<Message> GetOutbox(int userId);
        public void SendMessage(int fromUserId, int toUserId, Message message);
    }
    public class UserMessager: EntityRepository<Message>,IUserMessager
    {
        public UserMessager(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Message> GetInbox(int userId)
            => _context.Messages.Where(m => m.ToUserID == userId).ToArray();

        public IEnumerable<Message> GetOutbox(int userId)
            => _context.Messages.Where(m => m.FromUserID == userId).ToArray();

        public void SendMessage(int fromUserId, int toUserId, Message message)
        {
            message.FromUserID = fromUserId;
            message.ToUserID = toUserId;
            Post(message);
        }

        public override DbSet<Message> GetDbSet(ApplicationDbContext context)
            => context.Messages;
    }
}
