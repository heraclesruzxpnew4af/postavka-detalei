﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Auth.Services.UserMessager
{
    public class UserMessagerModule: IServiceModule
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<IUserMessager, UserMessager>();
        }
    }
}
