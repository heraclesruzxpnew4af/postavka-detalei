﻿using Auth.Data;

using Data.RepositoryPattern;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.UserManager
{
    public interface IUserManager
    {
        public int CreateUser(string Email, string Phone, string Password, string Name, string Surname, string Partonimyc, bool IsWoman, DateTime Birthday);
        public User FindByEmail(string Email);
        public User FindByPhone(string Phone);
    }
}
