﻿using Auth.Data;
using Auth.Services.AuthRepositories;

using Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.UserManager
{
    internal class UserManager: IUserManager
    {
        private readonly IUserRepository _users;
        private readonly ApplicationDbContext _context;
        private readonly IUserRoleRepository _userRoles;
        private readonly IRoleRepository _roles;
        private readonly IAccountRepository _accounts;
        private readonly IPersonRepository _persons;


        public User FindByEmail(string Email)
        {
            var user = _users.FindByEmail(Email);
            if(user != null)
            {
                user.Roles = _roles.ForUser(user.ID);
            }
            return user;
        }
        public User FindByPhone(string Phone)
        {
            var user = _users.FindByPhone(Phone);
            if (user != null)
            {
                user.Roles = _roles.ForUser(user.ID);
            }
            return user;
        }
       

        public UserManager(
                ApplicationDbContext context,
                IUserRoleRepository userRoles,
                IAccountRepository accounts,
                IRoleRepository roles,
                IUserRepository users,
                IPersonRepository persons)
        {
            _context = context;
            _userRoles = userRoles;
            _roles = roles;
            _users = users;
            _accounts = accounts;
            _persons = persons;
        }

   
        private string GetHash(string password) => password;

        public int CreateUser(string Email, string Phone, string Password, string Name, string Surname, string Partonimyc, bool IsWoman, DateTime Birthday)
        {
          
            var account = new Account()
            {
                Email = Email.ToUpper(),
                Phone = Phone,
                HashKey = GetHash(Password)
            };
            _context.Accounts.Add(account);
            _context.SaveChanges();

            var person = new Person()
            {
                Name = Name,
                Surname = Surname,
                Patronymic = Partonimyc,
                Birthday = Birthday,
                IsWoman = IsWoman
            };
            _context.Persons.Add(person);
            _context.SaveChanges();

            var user = new User()
            {
                Account = account,
                Person = person
                
            };
            _context.Users.Add(user);
            return _context.SaveChanges();


            

             
        }

         
    }
}
