﻿using Auth.Data;
using Auth.Services.AuthCertificate;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UserAuthMVC.Auth.Services;

namespace Auth.Services.SigninManager
{
    public class SigninManager: ISigninManager
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IAuthContext _authContext;
      
        public SigninManager(ITokenProvider tokenProvider, IAuthContext authContext)
        {
            _tokenProvider = tokenProvider;
            _authContext = authContext;
       
        }

        public bool IsSignIn()
        {
            string token = _tokenProvider.Get();
            return _authContext.IsSignIn(token);
        }

        public int GetUserID()
        {
            string token = _tokenProvider.Get();
            return _authContext.GetUserID(token);
        }

        public UserContext Signin(User user)
        {
            
            var userContext = _authContext.Signin(user);
            _tokenProvider.Set(userContext.Token);
            return userContext;
        }

        public bool Signout()
        {
            string token = _tokenProvider.Get();
            _tokenProvider.Set("");
            return _authContext.Signout(token);
        }

        public UserContext GetUserContext()
        {
            return _authContext.GetUserContext(_tokenProvider.Get());
        }

        public bool IsUserInRole(string area)
        {
            if(IsSignIn() == false)
            {
                return false;
            }
            else
            {
                var token = _tokenProvider.Get();
                var context = _authContext.GetUserContext(token);
                return context.User.Roles.Where(role => role.Code.ToLower().Equals(area)).Any();
            }
            
        }

        public string GetUserEmail()
        {
            var token = _tokenProvider.Get();
            var ctx = _authContext.GetUserContext(token);
            return ctx!=null?ctx.User.Account.Email: "";
        }
    }
}
