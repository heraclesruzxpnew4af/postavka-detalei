﻿using Auth.Data;

using Data.RepositoryPattern;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.SigninManager
{
    public interface ISigninManager
    {
        public UserContext Signin(User user);
        public bool IsSignIn();
        public int GetUserID();
        public string GetUserEmail();

        public UserContext GetUserContext();

        bool Signout();
        bool IsUserInRole(string area);
    }
}
