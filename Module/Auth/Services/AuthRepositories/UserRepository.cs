﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    internal class UserRepository: EntityRepository<Auth.Data.User>,IUserRepository
    {
        public override DbSet<User> GetDbSet(ApplicationDbContext context) => context.Users;
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public User FindByEmail(string Email)
        {
            var account = _context.Accounts.Where(account => account.Email.ToUpper() == Email.ToUpper()).FirstOrDefault();
            if (account == null)
            {
                return null;
            }
            else
            {
                var user = _context.Users
                    .Include(u=>u.Person)
                    .Include(u=>u.Account)
                    .Where(u=>u.Account.ID==account.ID)
                    .First();
                if (user == null)
                    throw new Exception($"Нарушение целостности данных каждой записи {nameof(Account)} должна соответсвовать запись {nameof(User)}");
            
                return user;
            }
        }

        public User FindByPhone(string Phone)
        {
            var account = _context.Accounts.Where(account => account.Phone.ToUpper() == Phone.ToUpper()).FirstOrDefault();
            if (account == null)
            {
                return null;
            }
            else
            {
                var user = _context.Users
                      .Include(u => u.Person)
                      .Include(u => u.Account)
                      .Where(u => u.Account.ID == account.ID)
                      .First();
                if (user == null)
                    throw new Exception($"Нарушение целостности данных каждой записи {nameof(Account)} должна соответсвовать запись {nameof(User)}");
                return user;
            }
        }
    }
}
