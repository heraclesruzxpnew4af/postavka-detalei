﻿using Data.RepositoryPattern;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    public interface IPersonRepository: IEntityRepository<Auth.Data.Person>
    {
    }
}
