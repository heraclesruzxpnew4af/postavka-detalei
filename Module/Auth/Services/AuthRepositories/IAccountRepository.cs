﻿using Auth.Data;

using Data.RepositoryPattern;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    
    public interface IAccountRepository : IEntityRepository<Auth.Data.Account>
    {
        public Account FindByEmail(string Email);
        public Account FindByPhone(string Email);
    }
}
