﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    internal class AccountRepository : EntityRepository<Auth.Data.Account>, IAccountRepository
    {
        public override DbSet<Auth.Data.Account> GetDbSet(ApplicationDbContext context) => context.Accounts;
        public AccountRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Account FindByEmail(string Email) => _context.Accounts.Where(account => account.Email.ToUpper() == Email.ToUpper()).First();

        public Account FindByPhone(string Phone) => _context.Accounts.Where(account => account.Phone.ToUpper() == Phone.ToUpper()).First();
    }
}
