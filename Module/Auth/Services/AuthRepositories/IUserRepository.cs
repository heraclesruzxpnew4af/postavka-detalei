﻿using Auth.Data;

using Data.RepositoryPattern;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    internal interface IUserRepository : IEntityRepository<Auth.Data.User>
    {
        public User FindByEmail(string Email);
        public User FindByPhone(string Email);
    }
}
