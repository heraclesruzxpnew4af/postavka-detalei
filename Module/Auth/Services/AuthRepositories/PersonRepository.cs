﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Services.AuthRepositories
{
    internal class PersonRepository: EntityRepository<Auth.Data.Person>, IPersonRepository
    {
        public override DbSet<Person> GetDbSet(ApplicationDbContext context) => context.Persons;
        public PersonRepository(ApplicationDbContext context) : base(context)
        {
        }

        
    }

    
}
