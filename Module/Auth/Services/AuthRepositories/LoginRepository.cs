﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

namespace Auth.Services.AuthRepositories
{
    internal class LoginRepository: EntityRepository<Auth.Data.Login>, ILoginRepository
    {
        public override DbSet<Login> GetDbSet(ApplicationDbContext context) => context.Logins;
        public LoginRepository(ApplicationDbContext context) : base(context)
        {
        }

        
    }
}