﻿using Auth.Data;

using Data;
using Data.RepositoryPattern;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;

namespace Auth.Services.AuthRepositories
{
    internal class RoleRepository : EntityRepository<Auth.Data.Role>, IRoleRepository
    {
        public override DbSet<Auth.Data.Role> GetDbSet(ApplicationDbContext context) => context.Roles;
        public RoleRepository(ApplicationDbContext context) : base(context)
        {
        }
        public ICollection<Role> ForUser(int userId)
        {
            var roleIds = _context.UserRoles.Where(ur => ur.UserID == userId).Select(ur => ur.RoleID).ToList();
            return _context.Roles.Where(r => roleIds.Contains(r.ID)).ToList();
        }

    }
    internal interface IUserRoleRepository
    {
        public void AddRoleForUser(int userId, int roleId);
    }
    internal class UserRoleRepository : EntityRepository<Auth.Data.UserRole>, IUserRoleRepository
    {
        public override DbSet<Auth.Data.UserRole> GetDbSet(ApplicationDbContext context) => context.UserRoles;
        public UserRoleRepository(ApplicationDbContext context) : base(context)
        {
        }
        public void AddRoleForUser(int userId, int roleId)
        {

            _context.UserRoles.Add(new UserRole() { 
                UserID = userId,
                RoleID = roleId
            });
            _context.SaveChanges();
        }

    }
}
