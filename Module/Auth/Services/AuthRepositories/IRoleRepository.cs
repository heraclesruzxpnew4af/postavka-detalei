﻿using Auth.Data;

using Data.RepositoryPattern;

using System.Collections.Generic;

namespace Auth.Services.AuthRepositories
{
    public interface IRoleRepository: IEntityRepository<Role>
    {
        public ICollection<Role> ForUser(int userId);
    }
}