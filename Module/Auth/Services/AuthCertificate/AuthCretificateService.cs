﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Auth.Services.AuthCertificate
{
    public class AuthCretificateService : IAuthCretificate
    {
        private readonly IHttpContextAccessor _accessor;

        public AuthCretificateService(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public async Task<X509Certificate2> GetCertificate()
        {
            return await _accessor.HttpContext.Connection.GetClientCertificateAsync();
        }
    }
}
