﻿using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Auth.Services.AuthCertificate
{
    public interface IAuthCretificate
    {
        Task<X509Certificate2> GetCertificate();
    }
}