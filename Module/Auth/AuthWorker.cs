﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
 

using Microsoft.Extensions.Hosting;

namespace Auth
{


    /// <summary>
    /// Периодически выполняет актулизацию сведений о сеансах пользователей
    /// </summary>
    public class AuthWorker: BackgroundService
    {
        private readonly IAuthContext _context;
        private readonly IAuthOptions _options;


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="console">консоль</param>
        /// <param name="context">контекст</param>
        public AuthWorker( IAuthOptions options, IAuthContext context )
        {
            _context = context;
            _options = options;
        }



        /// <summary>
        /// Собственно сама проверка
        /// </summary>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                
                _context.Validate();
                await Task.Delay(_options.ValidationTimeout, stoppingToken);
            }
        }
    }
}
