﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Auth.Singleton.ClientFactory
{
    public interface IClientFactory
    {
        public HttpClient Get();
    }
}
