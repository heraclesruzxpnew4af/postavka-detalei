﻿using Auth.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth
{
    public interface IAuthOptions
    {
        /// <summary>
        /// Длительность сеанса в секундах
        /// </summary>
        public int SessionTimeout { get; set; }

        /// <summary>
        /// Время периодической проверки в миличекундах
        /// </summary>
        public int ValidationTimeout { get; }
        public int KeyLength { get;  } 

        /// <summary>
        /// Роль по-умолчанию
        /// </summary>
        public string DefaultRole { get; }

        /// <summary>
        /// Путь к странице авторизации
        /// </summary>
        public string LoginPath { get; }
    }
    public class AuthOptions : IAuthOptions
    {
        /// <summary>
        /// Длительность сеанса в секундах
        /// </summary>
        public int SessionTimeout { get; set; } = 6;

        /// <summary>
        /// Частота проверки милисекундах
        /// </summary>
        public int ValidationTimeout { get; set; } = 1000;
        public int KeyLength { get; set; } = 32;

        public string DefaultRole => "User";
        public string LoginPath => "/Login";
    }
    
}
