﻿using Auth.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth
{
    /// <summary>
    /// Область памяти ассоциированая с активным объектом сеанса
    /// </summary>
    public class UserContext
    {
        public string Token { get; set; }
        public User User { get; set; }

        /// <summary>
        /// Unix-time последней операции
        /// </summary>
        public long LastActive { get; set; } = DateTimeOffset.Now.ToUnixTimeMilliseconds();
    }
}
