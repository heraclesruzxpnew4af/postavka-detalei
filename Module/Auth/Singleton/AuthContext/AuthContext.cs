﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Auth.Data;
using Auth.Singleton.ClientFactory;


using Microsoft.Extensions.Logging;

namespace Auth
{
    public interface IAuthContext
    {
        /// <summary>
        /// Выполнение проверки в милическундах
        /// </summary>
        void Validate();

        /// <summary>
        /// Проверка токена
        /// </summary>
        bool IsSignIn(string token);

        /// <summary>
        /// Получаем объект ассоциированный с сеансом 
        /// </summary>
        int GetUserID(string token);

        /// <summary>
        /// Получаем объект ассоциированный с сеансом 
        /// </summary>
        UserContext GetUserContext(string token);

        /// <summary>
        /// Создаём объект ассоциированный с сеансом 
        /// </summary>
        UserContext Signin(User user);

        /// <summary>
        /// Уничтожаем объект ассоциированный с сеансом 
        /// </summary>    
        bool Signout(string token);
        bool IsUserInRole(string v, string area);
    }
    public class AuthContext: IAuthContext
    {
        private readonly ILogger<AuthContext> _logger;
        private readonly IAuthOptions _options;
 
        private readonly Random random = new Random();






        /// <summary>
        /// Авторизованные пользователи
        /// </summary>
        private readonly ConcurrentDictionary<string, UserContext> users = new ConcurrentDictionary<string, UserContext>();


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="console"></param>
        public AuthContext(ILogger<AuthContext> logger, IAuthOptions options)
        {
            _logger = logger;
            _options = options;
          
        }

        public UserContext GetUserContext(string token) => users.ContainsKey(token)?users[token]: null;
        /// <summary>
        /// Проверка сеансов пользователя,  не активных ОТСЕЕВАЕМ НЕАКТИВНЫХ
        /// </summary>
        public void Validate()
        {
            //_logger.LogInformation($"Validate({GetNextToken()})");

            long time = DateTimeOffset.Now.ToUnixTimeSeconds(); 
            lock (users)
            {
                //_logger.LogInformation($"users count: {users.Count()}");
                
                foreach (var kv in users.ToArray())
                {
                    var userContext = kv.Value;

                    //_logger.LogInformation($"Validate user context: {userContext.ToString()}");
                    if ((time - userContext.LastActive) > _options.SessionTimeout)
                    {
                        Invalidate(kv);
                    }

                }
            }    
        }


        /// <summary>
        /// Освобождение памяти от не активного сеанса
        /// </summary>        
        private void Invalidate(KeyValuePair<string, UserContext> kv)
        {
            _logger.LogInformation($"Invalidate userContext: {kv.ToString()} {kv.Value.ToString()}");
            lock (users)
            {
                UserContext ctx;
                users.TryRemove(kv.Key, out ctx);
            }
        }

        private void Invalidate(string token)
        {
            _logger.LogInformation($"Invalidate userContext: {token} ");
            lock(users)
            {
                UserContext ctx;
                users.TryRemove(token, out ctx);
            }
        }

        public bool IsSignIn(string token) => 
            string.IsNullOrEmpty(token)? false: users.ContainsKey(token);

        public int GetUserID(string token)
        {
            if (users.ContainsKey(token))
            {
                lock (users)
                {
                    users[token].LastActive = DateTimeOffset.Now.ToUnixTimeSeconds();
                    return users[token].User.ID;
                }
            }
            else
            {
                return -1;
            }
            
        }

        public UserContext Signin(User user)
        {
            lock (users)
            {
                string token = CreateToken();
                users[token] = new UserContext()
                {
                    User = user,
                    Token = token
                };
                return users[token];
            }
        }

        private string CreateToken()
        {
            string token = null;
            do
            {
                token = GetNextToken();
            } while (users.ContainsKey(token));
            return token;
        }

        private string GetNextToken()
        {            
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToLower() +
                            "0123456789";
            return new string(Enumerable.Repeat(chars, _options.KeyLength)
                                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public bool Signout(string token)
        {
            var context = FindUser(token);
            if(context == null)
            {
                return false;
            }
            else
            {
                Invalidate(token);
                return true;
            }
        }

        public UserContext FindUser(string token)
        {
            lock (users)
            {
                if (users.ContainsKey(token))
                {
                    return users[token];
                }
                else
                {
                    return null;
                }
            }

        }

        public bool IsUserInRole(string token, string area)
        {
            var context = GetUserContext(token);
            return context.User.Roles.Where(role => role.Code.ToLower().Equals(area)).Any();
        }
    }
}
