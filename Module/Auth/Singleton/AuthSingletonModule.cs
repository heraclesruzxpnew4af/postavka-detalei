﻿ 
using Auth.Singleton.ClientFactory;


using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Singleton
{
    public class AuthSingletonModule: IServiceModule
    {
        private ClientFactoryModule ClientFactoryModule = new ClientFactoryModule();
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            ClientFactoryModule.ConfigureServices(configuration, services);
             
            services.AddSingleton (typeof(IAuthOptions),sp => {
                return configuration.GetSection(nameof(IAuthOptions)).Get<AuthOptions>();
            });
            //services.AddSingleton<IAuthOptions, AuthOptions>();
            services.AddSingleton<IAuthContext, AuthContext>();
            services.AddHostedService<AuthWorker>();
        }
    }
}
