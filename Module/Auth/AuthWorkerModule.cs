﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;

using UserAuthMVC;

namespace Auth
{

    /// <summary>
    /// Отвечает подключение AuthWorker
    /// </summary>
    internal class AuthWorkerModule: ServiceWorkerModule
    {
        private AuthServiceModule AuthServiceModule = new AuthServiceModule();

        /// <summary>
        /// Конфигурация служб авторизации работающих в фоновом режиме
        /// </summary>
        /// <param name="context"></param>
        /// <param name="services"></param>
        public override void ConfigureServices(IConfiguration configuration, IServiceCollection services )
        {

            
            AuthServiceModule.ConfigureServices(configuration, services);



        }
    }
}
