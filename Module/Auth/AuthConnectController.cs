﻿using Auth.Services.AuthCertificate;
using Auth.Singleton.ClientFactory;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Views.Deploy
{
    public class AuthConnectController : Controller
    {
        private IClientFactory _clientFactory;
        private readonly IAuthCretificate _authCretificate;

        public AuthConnectController(IClientFactory clientFactory, IAuthCretificate authCretificate)
        {
            _clientFactory = clientFactory;
            _authCretificate = authCretificate;
        }

        public string Test()
        {
            var client = _clientFactory.Get();
            var response = client.GetAsync("https://localhost:5001/Connect/Validate").Result;
            return response.Content.ReadAsStringAsync().Result;
        }


        public bool Validate()
        {
            var cert = _authCretificate.GetCertificate().Result;
            return cert != null;
        }
    }
}
