﻿using Auth.Data;
using Auth.Services.SigninManager;
using Auth.Services.UserMessager;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

namespace Areas.User.Controllers
{
    [Area("User")]
 
    public class MessagesController : Controller
    {
        private IUserMessager _userMessager;
        private readonly ISigninManager _signin;

        public MessagesController( ISigninManager signin, IUserMessager userMessager)
        {
            _userMessager = userMessager;
            _signin = signin;
        }

        public IActionResult Inbox() => View(GetInbox());
        public IActionResult Outbox() => View(GetOutbox());

        [HttpGet]
        public IActionResult Send() => View(new Message());

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _userMessager.Delete(id);
            return Redirect("/User/Messages/Inbox");
        }

        [HttpPost]
        public IActionResult Send(Message message)
        {
            if (ModelState.IsValid)
            {
                SendMessage(_signin.GetUserID(), message);
                return Redirect("/User/Messages/Inbox");
            }
            else
            {
                return View(message);
            }
        }

        IEnumerable<Message> GetInbox()
            => _userMessager.GetInbox(_signin.GetUserID());

        IEnumerable<Message> GetOutbox( )
            => _userMessager.GetOutbox(_signin.GetUserID());

        void SendMessage( int toUserId, Message message)
            => _userMessager.SendMessage(_signin.GetUserID(), toUserId, message);

    }
}
