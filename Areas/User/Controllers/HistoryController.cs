﻿using App.Areas.User.Data;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

namespace App.Areas.User.Controllers
{
    [Area("User")]
    public class HistoryController: Controller
    {
        public IActionResult Index()
        {
            return View(new List<FinOperation>() {
                new FinOperation(){ Created = System.DateTime.Now, OperationVolume = 1000 },
                new FinOperation(){ Created = System.DateTime.Now, OperationVolume = -100 },
                new FinOperation(){ Created = System.DateTime.Now, OperationVolume = -100 },
                new FinOperation(){ Created = System.DateTime.Now, OperationVolume = -100 }
            }) ;
        }
    }
}
