﻿using Microsoft.AspNetCore.Mvc;

namespace App.Areas.User.Controllers
{
 
    [Area("User")]
    public class ServicesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
