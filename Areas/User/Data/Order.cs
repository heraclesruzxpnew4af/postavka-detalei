﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MvcApplicaitonMarket.Areas.Market;

using System;
using System.Collections.Generic;

namespace MvcApplicaitonMarket.Areas.Market.Data
{
   
    public class Order : BaseEntity
    {
        public int ID { get; set; }
        private DateTime _orderDate;
         

        private readonly List<OrderItem> _orderItems;
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems;

        public object? DomainEvents { get; set; }
        public Address Address { get; set; }
        public int OrderStatus { get; set; }


        int buyerId;
        int paymentMethodId;
        Address address;

        protected Order() { }

        public Order(int buyerId, int paymentMethodId, Address address)
        {
            this.buyerId = buyerId;
            this.paymentMethodId = paymentMethodId;
            this.address = address;
        }

        public void AddOrderItem(int productId, string productName,
                                 decimal unitPrice, decimal discount,
                                 string pictureUrl, int units = 1)
        {
            // Validation logic...

            var orderItem = new OrderItem(productId, productName,
                                          unitPrice, discount,
                                          pictureUrl, units);
            _orderItems.Add(orderItem);
        }


        // At OrderEntityTypeConfiguration.cs from eShopOnContainers
        public class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
        {


            public void Configure(EntityTypeBuilder<Order> orderConfiguration)
            {
            

                //Address value object persisted as owned entity type supported since EF Core 2.0
                orderConfiguration
                    .OwnsOne(o => o.Address, a =>
                    {
                        a.WithOwner();
                    });

                orderConfiguration
                    .Property<int?>("_buyerId")
                    .UsePropertyAccessMode(PropertyAccessMode.Field)
                    .HasColumnName("BuyerId")
                    .IsRequired(false);

                orderConfiguration
                    .Property<DateTime>("_orderDate")
                    .UsePropertyAccessMode(PropertyAccessMode.Field)
                    .HasColumnName("OrderDate")
                    .IsRequired();

                orderConfiguration
                    .Property<int>("_orderStatusId")
                    .UsePropertyAccessMode(PropertyAccessMode.Field)
                    .HasColumnName("OrderStatusId")
                    .IsRequired();

                orderConfiguration
                    .Property<int?>("_paymentMethodId")
                    .UsePropertyAccessMode(PropertyAccessMode.Field)
                    .HasColumnName("PaymentMethodId")
                    .IsRequired(false);

                orderConfiguration.Property<string>("Description").IsRequired(false);
 
                orderConfiguration.HasOne<PaymentMethod>()
                    .WithMany()
                    .HasForeignKey("_paymentMethodId")
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Restrict);

                orderConfiguration.HasOne<Buyer>()
                    .WithMany()
                    .IsRequired(false)
                    .HasForeignKey("_buyerId");

                /*orderConfiguration.HasOne(o => o.OrderStatus)
                    .WithMany()
                    .HasForeignKey("_orderStatusId");*/
            }
        }
    }
}
