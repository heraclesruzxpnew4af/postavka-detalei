﻿using System.Collections.Generic;

namespace MvcApplicaitonMarket.Areas.Market.Data
{
    public class Buyer
    {

        public string FullName { get; set; }
        private List<PaymentMethod> _payments;
        public IReadOnlyCollection<PaymentMethod> Payments => _payments;
    }
}