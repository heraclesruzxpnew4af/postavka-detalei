﻿namespace MvcApplicaitonMarket.Areas.Market
{
    public class Basket
    {
        public int Id { get; internal set; }
        public object Items { get; internal set; }
        public string BuyerId { get; internal set; }
    }
}