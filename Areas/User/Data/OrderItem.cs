﻿namespace MvcApplicaitonMarket.Areas.Market.Data
{
    public class OrderItem
    {
        private int productId;
        private string productName;
        private decimal unitPrice;
        private decimal discount;
        private string pictureUrl;
        private int units;

        public OrderItem(int productId, string productName, decimal unitPrice, decimal discount, string pictureUrl, int units)
        {
            this.productId = productId;
            this.productName = productName;
            this.unitPrice = unitPrice;
            this.discount = discount;
            this.pictureUrl = pictureUrl;
            this.units = units;
        }
    }
}