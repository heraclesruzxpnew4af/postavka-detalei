﻿using Data.EntityTypes;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections;
using System.Collections.Generic;

namespace App.Areas.User.Data
{

    public class FinOperation: EventTable
    {
        public int UserWalletID { get; set; }
        public float OperationVolume { get; set; }
        public string OperationComment { get; set; }

        public override string[] Get()
            => new string[] { OperationVolume.ToString(), OperationComment };
    }
    public class UserWallet: DimTable
    {
        public ICollection<FinOperation> Operations { get; set; }
        public int UserID { get; set; }
        public float WalletVolume { get; set; }

    }
}
